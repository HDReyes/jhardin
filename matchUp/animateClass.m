//
//  animateClass.m
//  Boc
//
//  Created by Hajji on 4/15/16.
//  Copyright © 2016 Publicis-Manila. All rights reserved.
//

#import "animateClass.h"

@interface animateClass()
{
    UIView *popViewContainer;
    UIView *popView;
    UIView *overLayBlocker;
    CGSize stringSize;
    UILabel *messageLabel;
    UIButton *closeBtn;
    CGFloat width;
    CGFloat height;
    
}

@end

@implementation animateClass

- (id)init
{
    if(self == [super init]){
        
        // do some initialization here
        //
        width  = [UIScreen mainScreen].bounds.size.width;
        height = [UIScreen mainScreen].bounds.size.height;
        
        [self createPopUpView];
        
        
    }
    return self;
}

- (void)createPopUpView
{
    // overLayVlocker will render the background untouchable
    overLayBlocker          = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    [overLayBlocker setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:1]];
    //overLayBlocker.backgroundColor = [UIColor whiteColor];
    overLayBlocker.userInteractionEnabled = YES;

    // Initialize label
    messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(width/4, height/4, width / 2, 110)];
    //messageLabel.adjustsFontSizeToFitWidth = YES;
    messageLabel.font = [UIFont fontWithName:@"Counter-StrikeRegular" size:32.0f];
    //messageLabel.minimumScaleFactor = 30.0f/32.0f;
    messageLabel.clipsToBounds = YES;
    //messageLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    messageLabel.textColor = [UIColor darkGrayColor];
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.numberOfLines = 20;
    messageLabel.layer.borderWidth = 1.0f;
    messageLabel.layer.borderColor = [UIColor redColor].CGColor;
    // Add label as pop-up subview
    [overLayBlocker addSubview:messageLabel];
    
    // Initialize close button
    closeBtn = [[UIButton alloc] initWithFrame:CGRectMake(width/4, height/4, 100, 30)];
    [closeBtn setTitle:@"Close" forState:UIControlStateNormal];
    closeBtn.titleLabel.font = [UIFont fontWithName:@"Avenir-Heavy" size:12.0f];
    closeBtn.userInteractionEnabled = YES;
    //closeBtn.layer.borderWidth = 1.0f;
    //closeBtn.layer.borderColor = [UIColor redColor].CGColor;
    closeBtn.layer.cornerRadius = 2.0f;
    
    closeBtn.backgroundColor = [UIColor colorWithRed:0.196 green:0.847 blue:0.639 alpha:1];
    [closeBtn addTarget:self action:@selector(closePopUp) forControlEvents:UIControlEventTouchUpInside];
    
    [overLayBlocker addSubview:closeBtn];
}

- (void)popUpAnimationOpen:(UIView *)viewController
                 imageType:(int)type
                   message:(NSString *)message
                   referer:(id)referer
{
    //NSLog(@"ANIMATE");
    
    [viewController addSubview:overLayBlocker];
    messageLabel.text = message;
    
    float widthIs = [messageLabel.text
                     boundingRectWithSize: messageLabel.frame.size
                                  options: NSStringDrawingUsesLineFragmentOrigin
                               attributes: @{ NSFontAttributeName:messageLabel.font }
                                  context:nil].size.width;
    
    NSLog(@"the width of yourLabel is %f", widthIs);
    
    overLayBlocker.transform = CGAffineTransformMakeScale(0.8, 0.8);
    
    [UIView animateWithDuration:1.5 delay:0
         usingSpringWithDamping:0.2 initialSpringVelocity:0.1f
                        options:0 animations:^{
                                overLayBlocker.transform = CGAffineTransformIdentity;
                            } completion:nil];
}

- (void)closePopUp
{
    NSLog(@"HEY");
    [overLayBlocker removeFromSuperview];
//    popView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    
//    [UIView animateWithDuration:0.1 delay:0
//         usingSpringWithDamping:1.0 initialSpringVelocity:0.0f
//                        options:0 animations:^{
//                            popView.transform = CGAffineTransformMakeScale(0.1, 0.1);;
//                            //[popView removeFromSuperview];
//                            
//                        } completion:^(BOOL finished) {
//                            [overLayBlocker removeFromSuperview];
//                        }];
}
@end
