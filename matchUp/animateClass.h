//
//  animateClass.h
//  Boc
//
//  Created by Hajji on 4/15/16.
//  Copyright © 2016 Publicis-Manila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface animateClass : NSObject


- (void)popUpAnimationOpen:(UIView *)viewController
                 imageType:(int)type
                   message:(NSString *)message
                   referer:(id)referer;
- (void)closePopUp;
@end
