//
//  ViewController.m
//  matchUp
//
//  Created by Hajji on 4/11/16.
//  Copyright © 2016 Hajji. All rights reserved.
//

#import "ViewController.h"
@interface ViewController ()
{
    NSMutableArray *tileArray;
    NSMutableArray *imageViewArray;
    NSMutableArray *selectedTiles;
    int selectionOne;
    int selectionTwo;
    int movesLeft;
    int score;
    UIView *objOne;
    UIView *objTwo;
    //UIImageView *imageView;
    UIImage *tileImage;

    IBOutlet UIView *overlayView;
    IBOutlet UIButton *playBtn;
    IBOutlet UILabel *header;
    IBOutlet UILabel *prizeSpiel;
    IBOutlet UILabel *scoreSpiel;
    IBOutlet UILabel *timer;
    IBOutlet UILabel *gameStartsLabel;
    
    
    NSTimer *timerCnt;
    int countDown;
}

@property (weak, nonatomic) IBOutlet UIView *tileContainer;
@property (weak, nonatomic) IBOutlet UILabel *movesLeftLabel;
//@property (weak, nonatomic) IBOutlet UIView *overlayView;
//@property (weak, nonatomic) IBOutlet UILabel *message;

- (IBAction)startGame:(id)sender;

@end

@implementation ViewController

@synthesize tileContainer;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self startGame:nil];
//    tileArray       = [NSMutableArray new];
//    imageViewArray  = [NSMutableArray new];
//    selectedTiles   = [NSMutableArray new];
//    
//    movesLeft        = 10;
//    score            = 0;
//    int currentWidth = 2;
//    int nextLine     = 0;
//    
//    selectionOne = 0;
//    selectionTwo = 0;
//    
//    
//
//    NSLog(@"Tile Container H: %f - W: %f", tileContainer.frame.size.height, tileContainer.frame.size.width);
//    
//    //INITIALIZE VIEWS AND SHUFFLE ARRAY
//    tileArray = [self createViewArray];
//    
////    NSLog(@"ARR => %@", tileArray);
//    
//    for (UIView *tiles in tileArray) {
//                //NSLog(@"ARR ONJ -. %@", tiles);
//                [tiles setFrame:CGRectMake(currentWidth, nextLine, 175, 175)];
//                //NSLog(@"Current Width: %d",currentWidth);
//        
//                if (currentWidth == 542) {
//                     currentWidth = 2;
//                     nextLine += 180;
//                }else{
//                     //NSLog(@"First Line: %d", currentWidth);
//        
//                     currentWidth += 180;
//                }
//        
//                // SETS THE TILE LABEL
//                UILabel *tagNo = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
//                tagNo.text = [NSString stringWithFormat:@"%ld", (long)tiles.tag];
//        
//        
//                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 175, 175)];
//                tileImage = [UIImage imageNamed:[NSString stringWithFormat:@"%ld", (long)tiles.tag]];
//                imageView.image = tileImage;
//                [tiles addSubview:imageView];
////                tiles.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[NSString stringWithFormat:@"%d", tiles.tag]]];
//        [imageViewArray addObject:imageView];
//        
//                [tileContainer addSubview:tiles];
//    }
//    
//    [self performSelector:@selector(closeTiles) withObject:nil afterDelay:4.0];
    
}

// This will close all the opened tiles with 5sec delay
- (void)closeTiles{
    
//    for (UIView *tiles in tileArray) {
//        [self performSelector:@selector(closeInSequence:) withObject:tiles afterDelay:0.5];
//    }
    
//    NSLog(@"HERE %d", [tileArray count]);
//    for (int i = 0; i < [tileArray count]; i++) {
//        UIView *tile = [tileArray objectAtIndex:i];
//        NSLog(@"->%@",[tileArray objectAtIndex:i]);
//        [self performSelector:@selector(closeInSequence:) withObject:tile afterDelay:i/100];
//    }
//        for (int i = 0; i < [tileArray count]; i++) {
//            [UIView transitionWithView:[tileArray objectAtIndex:i] duration:0.5
//                               options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
//                                   UIImageView *img = [imageViewArray objectAtIndex:i];
//                                   img.image = [UIImage imageNamed:@"back"];
//                                   
//                               } completion:nil];
//        }
    for (int x = 0; x < [tileArray count]; x++) {
        UIView *tile = [tileArray objectAtIndex:x];
        for (UIView *i in tile.subviews){
            if([i isKindOfClass:[UIImageView class]]){
                [UIView transitionWithView:tile duration:0.5
                                   options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
                                       UIImageView *img = (UIImageView *)i;
                                       img.image = [UIImage imageNamed:@"back"];
                UITapGestureRecognizer *tapToOpen = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                            action:@selector(openTile:)];
                                       
                                       // Add gesture
                [tile addGestureRecognizer:tapToOpen];
                                       
                } completion:nil];
                
            }
        }
        
    }
}

- (void)closeInSequence:(UIView *)tile
{
    
//    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 125, 125)];
//    secondImage = [UIImage imageNamed:@"emma-stone"];
//    [imageView removeFromSuperview];
    [UIView transitionWithView:tile duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
//                           imageView.image = secondImage;
                           tile.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back"]];
                           
                       } completion:nil];

}

// This will generate the random array
- (NSMutableArray *)randomNumber{
    NSMutableArray *randArray = [NSMutableArray new];
    
    for (int i = 1; i <= 20; i++) {
        [randArray addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
//    for (int x = 0; x < [randArray count]; x++) {
//        int randInt = (arc4random() % ([randArray count] - x)) + x;
//        [randArray exchangeObjectAtIndex:x withObjectAtIndex:randInt];
//    }
    NSLog(@"Here -> %@", randArray);

    return randArray;
}

- (NSMutableArray *)createViewArray{
    
    NSMutableArray *viewArray = [NSMutableArray new];
    
    for (int i = 1; i <= 20; i++) {
        
        UIView *tiles = [UIView new];
        tiles.tag = i;
        //tiles.backgroundColor = [UIColor whiteColor];
        //tiles.layer.borderColor = [UIColor redColor].CGColor;
        tiles.layer.borderWidth = 0.5f;
        
        [viewArray addObject:tiles];
        
    }
    
    // THIS WILL SHUFFLE THE VIEWS ARRAY
    for (int x = 0; x < [viewArray count]; x++) {
        int randInt = (arc4random() % ([viewArray count] - x)) + x;
        [viewArray exchangeObjectAtIndex:x withObjectAtIndex:randInt];
    }
    return viewArray;
}


- (void)openTile:(UITapGestureRecognizer *)recognizer {
    //Do stuff here...
    UIView *tagID = recognizer.view;
    //NSLog(@"TAG: %ld", (long)tagID.tag);
    int tagNo = (int)tagID.tag;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"tag == %d", tagNo];
    NSArray *filteredArray = [tileArray filteredArrayUsingPredicate:predicate];
    //NSLog(@"VALUE OF SELECTED: %@", [filteredArray objectAtIndex:0]);
    
    //Check if tapped UIView is on selectedTilesArray
    NSPredicate *tilePredicate = [NSPredicate predicateWithFormat:@"tag == %d", tagNo];
    NSArray *alreadySelected = [selectedTiles filteredArrayUsingPredicate:tilePredicate];
    //NSLog(@"VALUE OF SELECTED: %d", [alreadySelected count]);

    // BALANCE OUT THE SELECTED VALUES BY ADDING 10 TO NUMBER BELOW 10
    int balanceTagNo = 0;
    
    if (tagNo <= 10) {
        balanceTagNo = tagNo + 10;
    }else{
        balanceTagNo = tagNo;
    }
    //NSLog(@"SELECTION ONE %d - SELECTION TWO %d", selectionOne, selectionTwo);
    // ASSIGN SELECTED VALUES TO GLOBAL VARIABLE HOLDERS
    if ([alreadySelected count] == 0 || alreadySelected == nil) {
    
        if (selectionOne == 0) {
            selectionOne = balanceTagNo;
            objOne = [filteredArray objectAtIndex:0];
            [objOne setUserInteractionEnabled:FALSE];
            //objOne.backgroundColor = [UIColor redColor];
            //[testing addSubview:imageView];
            
            //        if ([alreadySelected count] == 0) {
            //NSLog(@"INTEGER OF SELECTED: %@", [alreadySelected objectAtIndex:0]);
            NSLog(@"ss");
            [self flipRight:objOne addCall:^(BOOL status) {
                
                NSLog(@"%hhd", status);
                
                
            }];
            //        }
            //        [UIView transitionWithView:objOne duration:0.5
            //                           options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
            //                               objOne.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[NSString stringWithFormat:@"%d", tagNo]]];
            //                               if ([alreadySelected count] > 0) {
            //                                   NSLog(@"INTEGER OF SELECTED: %@", [alreadySelected objectAtIndex:0]);
            //                               }
            //
            ////                               UIImageView *img = (UIImageView *)objOne.subviews;
            ////                               img.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d", tagID.tag]];
            //
            //                               for (UIView *i in objOne.subviews){
            //                                   if([i isKindOfClass:[UIImageView class]]){
            //                                       UIImageView *img = (UIImageView *)i;
            //                                       img.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d", tagNo]];
            //                                   }
            //                               }
            //
            //                           } completion:nil];
            //[self flipRight:objOne];
            
            
        }else if(selectionTwo == 0){
            selectionTwo = balanceTagNo;
            objTwo = [filteredArray objectAtIndex:0];
            [objTwo setUserInteractionEnabled:FALSE];
            
            //[self flipRight:objTwo];
            
            //objTwo.backgroundColor = [UIColor redColor];
            //        [UIView transitionWithView:objTwo duration:0.5
            //                           options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
            ////                               objTwo.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[NSString stringWithFormat:@"%d", tagID.tag]]];
            //
            //                               for (UIView *i in objTwo.subviews){
            //                                   if([i isKindOfClass:[UIImageView class]]){
            //                                       UIImageView *img = (UIImageView *)i;
            //                                       img.image = [UIImage imageNamed:[NSString stringWithFormat:@"%ld", (long)tagID.tag]];
            //                                   }
            //                               }
            //
            //
            //                           } completion:^(BOOL finished) {
            //
            //                               // CHECK IF TWO GLOBAL VARIABLES ARE OF THE SAME VALUE IF NOT RETURN BACK TO 0
            //                               if ((selectionOne != 0) && (selectionTwo != 0)) {
            //                                   if (selectionOne == selectionTwo) {
            //                                       NSLog(@"WOOHOO!!!! Global Selected %d -- %d", selectionOne, selectionTwo);
            //                                       [selectedTiles addObject:objOne];
            //                                       [selectedTiles addObject:objTwo];
            //
            //                                   }else{
            //                                       NSLog(@"SORRY!!!! Global Selected %d -- %d", selectionOne, selectionTwo);
            //                                       [self flipLeft:objOne];
            //                                       [self flipLeft:objTwo];
            //                                   }
            //
            //                                   // RESET INITIAL TO 0
            //                                   selectionOne = 0;
            //                                   selectionTwo = 0;
            //                                   [objOne setUserInteractionEnabled:TRUE];
            //                                   [objTwo setUserInteractionEnabled:TRUE];
            //                               }
            //
            //                           }];
            
            //if ([alreadySelected count] == 0) {
            [self flipRight:objTwo addCall:^(BOOL status) {
                
                if (status) {
                    // CHECK IF TWO GLOBAL VARIABLES ARE OF THE SAME VALUE IF NOT RETURN BACK TO 0
                    if ((selectionOne != 0) && (selectionTwo != 0)) {
                        if (selectionOne == selectionTwo) {
                            //NSLog(@"WOOHOO!!!! Global Selected %d -- %d", selectionOne, selectionTwo);
                            [selectedTiles addObject:objOne];
                            [selectedTiles addObject:objTwo];
                            
                            score += 1;
                            
                        }else{
                            //NSLog(@"SORRY!!!! Global Selected %d -- %d", selectionOne, selectionTwo);
                            [self flipLeft:objOne];
                            [self flipLeft:objTwo];
                        }
                        
                        // RESET INITIAL TO 0
                        selectionOne = 0;
                        selectionTwo = 0;
                        [objOne setUserInteractionEnabled:TRUE];
                        [objTwo setUserInteractionEnabled:TRUE];
                        
                        movesLeft -= 1;
                        [self checkResult];
                    }
                }
                
                
            }];
            //}
            
            
        }
        
    }

}

- (void)checkResult{
    NSLog(@"Score: %d Moves Left %d", score, movesLeft);
    _movesLeftLabel.text = [NSString stringWithFormat:@"%d", movesLeft];
    //[animationClass popUpAnimationOpen:self.view imageType:1 message:@"You have chosen the Shipment Baggage Option" referer:self];
    
    if(movesLeft <= 0){
        
        if (score < 3) {
            header.text = @"Sorry";
            scoreSpiel.text = @"You have reached\nthe maximum moves!";
            prizeSpiel.text = @"Thank you for participating.";
            
        }else{
            header.text = @"Congratulations!";
            scoreSpiel.text = [NSString stringWithFormat:@"You got %d pairs correct!", score];
            prizeSpiel.text = @"Please claim your prize through our representative!";
        }
        
        //[self performSelector:@selector(popResult) withObject:nil afterDelay:1.0];
        overlayView.hidden  = NO;
        header.hidden       = NO;
        prizeSpiel.hidden   = NO;
        scoreSpiel.hidden   = NO;
        playBtn.hidden      = NO;
        
        overlayView.transform = CGAffineTransformMakeScale(0.5, 0.5);
        [UIView animateWithDuration:1.5 delay:0
             usingSpringWithDamping:0.2 initialSpringVelocity:0.1f
                            options:0 animations:^{
                                overlayView.transform = CGAffineTransformIdentity;
                            } completion:nil];
        
    }else{
        if(score == 3){
            header.text = @"Congratulations!";
            scoreSpiel.text = [NSString stringWithFormat:@"You got %d pairs correct!", score];
            prizeSpiel.text = @"Please claim your prize through our representative!";
            
            //[self performSelector:@selector(popResult) withObject:nil afterDelay:1.0];
            overlayView.hidden  = NO;
            header.hidden       = NO;
            prizeSpiel.hidden   = NO;
            scoreSpiel.hidden   = NO;
            playBtn.hidden      = NO;
            
            overlayView.transform = CGAffineTransformMakeScale(0.5, 0.5);
            [UIView animateWithDuration:1.5 delay:0
                 usingSpringWithDamping:0.2 initialSpringVelocity:0.1f
                                options:0 animations:^{
                                    overlayView.transform = CGAffineTransformIdentity;
                                } completion:nil];
        }

    }
}

- (void)popResult{
}

- (void)flipRight:(UIView *)view addCall:(void (^)(BOOL status))callback;
{
        [UIView transitionWithView:view duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromRight animations:^{
                           
                           for (UIView *i in view.subviews){
                               if([i isKindOfClass:[UIImageView class]]){
                                   UIImageView *img = (UIImageView *)i;
                                   img.image = [UIImage imageNamed:[NSString stringWithFormat:@"%ld", (long)view.tag]];
                               }
                           }
                       } completion:^(BOOL finished) {
                           //return finished;
                           callback(finished);
                           
                       }];
}

- (void)flipLeft:(UIView *)view
{
    [UIView transitionWithView:view duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
                           //objOne.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"back"]];
                           for (UIView *i in view.subviews){
                               if([i isKindOfClass:[UIImageView class]]){
                                   UIImageView *img = (UIImageView *)i;
                                   img.image = [UIImage imageNamed:@"back"];
                               }
                           }
                           
                       } completion:nil];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
}

- (IBAction)startGame:(id)sender
{
    header.hidden       = YES;
    prizeSpiel.hidden   = YES;
    scoreSpiel.hidden   = YES;
    playBtn.hidden      = YES;
    
    timer.hidden            = NO;
    gameStartsLabel.hidden  = NO;
    overlayView.hidden      = NO;
//    overlayView.transform = CGAffineTransformMakeScale(0.8, 0.8);
//    [UIView animateWithDuration:1.5 delay:0
//         usingSpringWithDamping:0.2 initialSpringVelocity:0.1f
//                        options:0 animations:^{
//                            overlayView.transform = CGAffineTransformIdentity;
//                        } completion:nil];
   
    countDown  = 5;
    timer.text = [NSString stringWithFormat:@"%d", countDown];
    timerCnt  = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
    
}

- (void)countDown
{
    countDown -= 1;
    timer.text = [NSString stringWithFormat:@"%d", countDown];
    
    if (countDown == 0) {
        countDown = 5;
        timer.hidden            = YES;
        gameStartsLabel.hidden  = YES;
        overlayView.hidden      = YES;
        
        [timerCnt invalidate];
        [self loadGame];
    }
}

- (void)loadGame
{
    // Do any additional setup after loading the view, typically from a nib.
    if ([tileArray count] > 0) {
        for (int g=0; g<[tileArray count]; g++) {
            UIView *gg = [tileArray objectAtIndex:g];
            [gg removeFromSuperview];
        }
    }
    
    [objOne removeFromSuperview];
    [objTwo removeFromSuperview];
    tileArray       = nil;
    imageViewArray  = nil;
    selectedTiles   = nil;
    
    tileArray       = [NSMutableArray new];
    imageViewArray  = [NSMutableArray new];
    selectedTiles   = [NSMutableArray new];
    
    movesLeft        = 10;
    score            = 0;
    int currentWidth = 2;
    int nextLine     = 0;
    
    selectionOne = 0;
    selectionTwo = 0;
    
    _movesLeftLabel.text = @"10";

    
    NSLog(@"Tile Container H: %f - W: %f", tileContainer.frame.size.height, tileContainer.frame.size.width);
    
    //INITIALIZE VIEWS AND SHUFFLE ARRAY
    tileArray = [self createViewArray];
    
    //    NSLog(@"ARR => %@", tileArray);
    
    for (UIView *tiles in tileArray) {
        //NSLog(@"ARR ONJ -. %@", tiles);
        [tiles setFrame:CGRectMake(currentWidth, nextLine, 175, 175)];
        NSLog(@"Current Width: %d",currentWidth);
        
        if (currentWidth == 542) {
            currentWidth = 2;
            nextLine += 180;
        }else{
            //NSLog(@"First Line: %d", currentWidth);
            
            currentWidth += 180;
        }
        
        // SETS THE TILE LABEL
        UILabel *tagNo = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
        tagNo.text = [NSString stringWithFormat:@"%d", tiles.tag];
        
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 175, 175)];
        tileImage = [UIImage imageNamed:[NSString stringWithFormat:@"%d", tiles.tag]];
        imageView.image = tileImage;
        [tiles addSubview:imageView];
        //                tiles.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[NSString stringWithFormat:@"%d", tiles.tag]]];
        [imageViewArray addObject:imageView];
        
        [tileContainer addSubview:tiles];

//        UITapGestureRecognizer *tapToOpen = [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                                                    action:@selector(openTile:)];
//        
//        // Add gesture
//        [tiles addGestureRecognizer:tapToOpen];
    }
    
    [self performSelector:@selector(closeTiles) withObject:nil afterDelay:4.0];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
